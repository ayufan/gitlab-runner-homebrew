class GitLabRunner < Formula
  desc "GitLab Runner"
  homepage "https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/"
  url "https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/repository/archive.tar.gz?ref=v0.5.2"
  sha256 "3bf4471e3b8a94415c8012184ee8d550ffc95e8541ff28ac52f7269d035eb196"
  head "https://gitlab.com/gitlab-org/gitlab-ci-multi-runner.git"
  version "0.5.2"

  bottle do
    cellar :any
    sha256 "01cc46b0f7f607a44430f51bb7c901cac830a7b90eaa3cabbe80978e3a1a07e9" => :yosemite
    sha256 "5bfc32625aa30c30a90a2f7884deb3f6a570af30a8258247c6fdb602c7a9aeec" => :mavericks
    sha256 "e119b92ef81a31b0ccafec451045dda89d1094eb5308dcfa6f2fb468189da45f" => :mountain_lion
  end

  depends_on "git"
  depends_on "go" => :build

  go_resource "github.com/tools/godep" do
    url "https://github.com/tools/godep.git", :revision => "58d90f262c13357d3203e67a33c6f7a9382f9223"
  end

  def install
    ENV["GOPATH"] = buildpath
    mkdir_p buildpath/"src/gitlab.com/gitlab-org/"
    ln_sf buildpath, buildpath/"src/gitlab.com/gitlab-org/gitlab-ci-multi-runner"
    Language::Go.stage_deps resources, buildpath/"src"

    cd "src/github.com/tools/godep" do
      system "go", "install"
    end

    system "./bin/godep", "go", "build", "-o", "docker-machine", "."
    bin.install "gitlab-ci-multi-runner"
    bin.install_symlink "gitlab-runner"
  end

  test do
    system bin/"gitlab-runner", "-v"
  end
end
